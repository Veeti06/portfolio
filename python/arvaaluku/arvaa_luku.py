import random

while True:
    # generate a random number between 1 and 10
    number = random.randint(1, 10)
    
    # initialize the number of guesses to 0
    num_guesses = 0
    
    # loop until the user runs out of guesses or guesses the correct number
    while num_guesses < 3:
        # prompt the user to guess the number
        guess = int(input("Guess a number between 1 and 10: "))
        
        num_guesses += 1
        
        # check if the guess is too high, too low, or correct
        if guess < number:
            print("Too low!")
        elif guess > number:
            print("Too high!")
        else:
            print("Congratulations, you guessed the number in", num_guesses, "guesses!")
            num_guesses = 3  # exit the loop since the user has won
            break
    
    # check if the user ran out of guesses and didn't win
    if num_guesses == 3 and guess != number:
        print("Sorry, you ran out of guesses. The number was", number)
    elif num_guesses < 3:
        print("You have won!")
    
    # ask if the user wants to play again
    play_again = input("Do you want to play again? (y/n): ")
    
    if play_again.lower() != "y":
        break
