function kortin_arvot(nimi, ika, email, radio) {
    let otsikko = document.getElementById("nimi");
    otsikko.innerHTML = nimi;
    let ikaspan = document.getElementById("ika");
    ikaspan.innerHTML = ika;
    let emailspan = document.getElementById("email");
    emailspan.innerHTML = email;
    let radiospan = document.getElementById("radio")
    radiospan.innerHTML = radio;
}

function parametrit() {
    const queryString = window.location.search;
    console.log(queryString);
    const urlParams = new URLSearchParams(queryString);
    const nimi = urlParams.get('nimi')
    console.log(nimi);
    const ika = urlParams.get('ika')
    console.log(ika)
    const email = urlParams.get('email')
    console.log(email)
    const radio = urlParams.get('joku_radio')
    kortin_arvot(nimi, ika, email, radio)
}

window.onload = parametrit